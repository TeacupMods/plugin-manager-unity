﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

//using Log = TeaTop.Commands.AConsoleTest.Log;

namespace TeaTop.PluginSystem.Default
{
    public class TeaConsole : Plugin
    {
        public static TeaConsole Instance;
        private PluginWindow console;
        private PluginWindow consoleAutocomplete;
        private Vector2 consoleScroll;
        private static List<Log> logs = new List<Log>();
        private static string consoleInput = "";
        private static bool scrollToBottom = false;
        private bool scrollingToBottom = true;
        private bool drawAutoComplete = true;
        private float prevHeight = 0f;
        private bool autocompleteEmpty = false;

        public static List<string> inputHistory = new List<string>();
        public static int historyIndex = 0;

        public static List<KeyCode> keysDown = new List<KeyCode>();

        public static void AddLog(string log)
        {
            logs.Add(new Log(log));
            scrollToBottom = true;
        }

        public static void AddLog(string log, Color color)
        {
            logs.Add(new Log(log, color));
            scrollToBottom = true;
        }


        private bool takeFocus = false;
        private bool gotoEnd = false;
        private bool keyPress = false;
        /// <inheritdoc />
        public override void Load()
        {
            Instance = this;
            var gradient = new Gradient();

            // Populate the color keys at the relative time 0 and 1 (0 and 100%)
            var colorKey = new GradientColorKey[2];
            colorKey[0].color = new Color(0f, 0f, 0f);
            colorKey[0].time = 0.0f;
            colorKey[1].color = new Color(0.35f, 0.35f, 0.35f);
            colorKey[1].time = 1.0f;

            // Populate the alpha  keys at relative time 0 and 1  (0 and 100%)
            var alphaKey = new GradientAlphaKey[2];
            alphaKey[0].alpha = 1.0f;
            alphaKey[0].time = 0.0f;
            alphaKey[1].alpha = 0.35f;
            alphaKey[1].time = 1.0f;

            gradient.SetKeys(colorKey, alphaKey);

            console = AddWindow("Console", new PluginWindow()
            {
                NoButton = true,
                Draggable = true,
                Visible = true,
                Title = "",
                OnDraw = OnDraw,
                DefaultWindow = new Rect(Screen.width / 2, 25, 700, 500),
                GUILayout = false,
                WindowStyle = new GUIStyle()
                {
                    normal =
                    {
                        background = TeaUtils.MakeTex(new Color(0f, 0f, 0f, 0.6f)),
                    },
                    onHover =
                    {
                        background = TeaUtils.MakeTex(new Color(0f, 0f, 0f, 0.85f)),
                    },
                    richText = true,
                    clipping = TextClipping.Overflow,
                    wordWrap = false
                }
            });
            this.consoleAutocomplete = AddWindow("ConsoleAutocomplete", new PluginWindow()
            {
                NoButton = true,
                Draggable = false,
                Visible = this.console.Visible,
                OnDraw = DrawAutocomplete,
                DefaultWindow = new Rect(this.console.Window.x, this.console.Window.y + this.console.Window.height, 700, 10),
                WindowStyle = new GUIStyle()
                {
                    normal =
                    {
                        background = TeaUtils.MakeTex(1, 1, new Color(0, 0, 0, 0))
                    }
                }
            });

            //AccessTools.Method(typeof(InputField), "KeyPressed").HarmonyPatch(GetPatchMethod("NoMoveUpDown"));
            //Logging.LogInfo("Console window created yay\n\nlmaofeg");
            //this.logs.Add("Console Start");
        }

        private void DrawAutocomplete(PluginWindow window)
        {
            if (!drawAutoComplete || !this.console.IsFocused() && !window.IsFocused())
                return;

            if (this.console.IsFocused() || window.IsFocused())
            {
                window.WindowStyle.normal.background = TeaUtils.MakeTex(1, 1, new Color(0f, 0f, 0f, 0.85f));
            }
            else
            {
                window.WindowStyle.normal.background = TeaUtils.MakeTex(1, 1, new Color(0f, 0f, 0f, 0f));
            }

            GUILayout.Space(-3);

            if (String.IsNullOrEmpty(consoleInput) && !autocompleteEmpty)
                return;

            string cmdtxt = consoleInput.Split(' ')[0];
            if (String.IsNullOrEmpty(cmdtxt) && !autocompleteEmpty)
                return;

            GUIStyle style = new GUIStyle(GUI.skin.label);
            style.normal.textColor = new Color(1f, 0.3f, 0.3f);
            style.fontSize = 15;
            style.richText = true;

            int count = 0;
            foreach (PluginCommand cmd in this.autocompleteEmpty ? CommandManager.GetCommandsSortedByName() : CommandManager.GetCommandsSortedByLength())
            {
                string txt = cmd.Command;
                if (txt.Length < cmdtxt.Length && !this.autocompleteEmpty)
                {
                    continue;
                }
                if ((txt.StartsWith(consoleInput) || this.autocompleteEmpty) && count < 10)
                {
                    GUILayout.Label($"<b>{cmd.Command}</b> {cmd.HelpText}", style);
                    GUILayout.Space(-7);
                    count++;
                }
            }
        }

        private void OnDraw(PluginWindow window)
        {
            bool takeFocus = this.takeFocus;
            if (takeFocus)
            {
                window.GrabFocus();
                this.takeFocus = false;
            }

            var pos = Event.current.mousePosition;
            GUI.Label(new Rect(5, 0, 100, 20), "<b>Tea Console</b>");
            TeaGUI.Separator(new Rect(0, 18, 700, 1));
            GUIStyle bstyle = new GUIStyle(GUI.skin.button);
            bstyle.normal.background = TeaUtils.MakeTex(1, 1, new Color(0.65f, 0.65f, 0.65f, 0.65f));
            bstyle.hover.background = TeaUtils.MakeTex(1, 1, new Color(0.65f, 0.65f, 0.65f, 1f));
            bstyle.hover.textColor = Color.white;
            bstyle.normal.textColor = Color.white;
            if (GUI.Button(new Rect(5, 22, 100, 20), "Clear", bstyle))
            {
                logs.Clear();
            }

            if (GUI.Button(new Rect(110, 22, 100, 20), "Add Log", bstyle))
            {
                AddLog($"Prev height: {this.prevHeight} Prev scroll: {this.consoleScroll.y}");
            }

            TeaGUI.Toggle(new Rect(215, 22, 150, 20), ref this.scrollingToBottom, "Scroll To Bottom");
            TeaGUI.Toggle(new Rect(330, 22, 150, 20), ref this.drawAutoComplete, "Draw Auto-Complete");
            TeaGUI.Separator(new Rect(0, 45, 700, 1));

            float scrollHeight = 0;
            var labelHeights = new List<Tuple<float, Log>>();
            int i = 0;
            logs.ForEach(s =>
            {
                var logBoxHeight = Mathf.Min(100f, GUI.skin.label.CalcHeight(new GUIContent(s.txt), 690));
                labelHeights.Add(Tuple.Create(logBoxHeight, s));
                //labelHeights[i] = logBoxHeight;
                i++;

                scrollHeight += logBoxHeight - 3f;
            });

            if (this.scrollingToBottom && scrollToBottom)
            {
                consoleScroll = new Vector2(0, scrollHeight);
                scrollToBottom = false;
            }

            consoleScroll = GUI.BeginScrollView(new Rect(5, 50, 690, 420), consoleScroll, new Rect(0, 0, 673, scrollHeight), false, false);
            i = 0;
            float currentY = 0f;
            foreach (Tuple<float, Log> log in labelHeights)
            {
                float height = log.Item1;
                GUI.Label(new Rect(0, currentY, 673, height), log.Item2.txt, new GUIStyle(GUI.skin.label) { normal = { textColor = log.Item2.color } });
                i++;
                currentY += height - 3f;

            }
            //GUI.Label(new Rect(0, 5, 680, size), drawString);
            GUI.EndScrollView();

            // Input Field Below

            GUI.SetNextControlName("consoleInput");
            if (GUI.GetNameOfFocusedControl() != "consoleInput")
            {
                if (Event.current.type == EventType.Repaint && string.IsNullOrEmpty(consoleInput))
                {
                    GUIStyle style = new GUIStyle(GUI.skin.textField);
                    style.normal.textColor = new Color(0.5f, 0.5f, 0.5f, 0.75f);
                    GUI.TextField(new Rect(5, 475, 690, 20), "Enter a command", style);
                }
                else
                {
                    GUI.TextField(new Rect(5, 475, 690, 20), consoleInput);
                }
            }
            else
            {
                if (Event.current.isKey)
                {
                    if (Event.current.type == EventType.KeyDown)
                    {
                        switch (Event.current.keyCode)
                        {
                            case KeyCode.UpArrow:
                                if (inputHistory.Count > 0)
                                {

                                    /*Logging.LogInfo($"hist count: {inputHistory.Count}");
                                    Logging.LogInfo($"idx: {historyIndex} next hist: {inputHistory[historyIndex]}");*/
                                    consoleInput = inputHistory[historyIndex];
                                    this.gotoEnd = true;
                                    historyIndex++;
                                    if (historyIndex >= inputHistory.Count)
                                    {
                                        historyIndex = inputHistory.Count - 1;
                                    }
                                }

                                break;
                            case KeyCode.DownArrow:
                                if (inputHistory.Count > 0 && historyIndex != 0)
                                {
                                    historyIndex--;
                                    if (historyIndex < 0)
                                    {
                                        historyIndex = 0;
                                    }

                                    consoleInput = inputHistory[historyIndex];
                                    this.gotoEnd = true;
                                }

                                break;
                            case KeyCode.Return:
                            case KeyCode.KeypadEnter:
                                if (!String.IsNullOrEmpty(consoleInput))
                                {
                                    ProcessInput(consoleInput);
                                    consoleInput = "";
                                    Event.current.Use(); // Ignore event, otherwise there will be control name conflicts!
                                }

                                break;
                            case KeyCode.Tab:
                                if (!String.IsNullOrEmpty(consoleInput))
                                {
                                    var cmds = CommandManager.GetCommandsSortedByLength();
                                    var cmd = cmds.FirstOrDefault(x => x.Command != consoleInput && x.Command.StartsWith(consoleInput.ToLower()));
                                    if (cmd != null)
                                    {
                                        historyIndex = 0;
                                        consoleInput = cmd.Command;
                                        this.gotoEnd = true;
                                        Event.current.Use();
                                    }
                                }
                                else
                                {
                                    if (!keysDown.Contains(KeyCode.Tab))
                                    {
                                        historyIndex = 0;
                                        autocompleteEmpty = !autocompleteEmpty;
                                    }
                                }

                                break;
                            case KeyCode.BackQuote:
                                GUI.FocusWindow(-1);
                                GUI.FocusControl("");
                                window.Visible = false;
                                this.consoleAutocomplete.Visible = this.console.Visible;
                                break;
                        }
                    }

                    if (Event.current.type == EventType.KeyDown && Event.current.keyCode != KeyCode.None)
                    {
                        if (Event.current.keyCode != KeyCode.Tab)
                            this.autocompleteEmpty = false;

                        keysDown.Add(Event.current.keyCode);
                    }

                    if (Event.current.type == EventType.KeyUp && keysDown.Contains(Event.current.keyCode) && Event.current.keyCode != KeyCode.None)
                    {
                        keysDown.Remove(Event.current.keyCode);
                    }
                }

                consoleInput = GUI.TextField(new Rect(5, 475, 690, 20), consoleInput);

                if (gotoEnd)
                {
                    var textEditor = TeaGUI.GetStateObject<TextEditor>();
                    int num1 = consoleInput.Length;
                    if (textEditor.cursorIndex == num1)
                        gotoEnd = false;

                    textEditor.cursorIndex = num1;
                    textEditor.selectIndex = num1;
                    //gotoEnd = false;
                }
            }

            if (takeFocus)
            {
                GUI.FocusControl("consoleInput");
            }
        }

        public void ProcessInput(string text)
        {
            AddLog("> " + text);
            if (text.ToLower() == "clear" || text.ToLower() == "cls")
            {
                logs.Clear();
                this.consoleScroll = Vector2.zero;
            }
            else if (!CommandManager.HandleCommand(text, false))
            {
                AddLog("Invalid command.", Color.red);
            }

            historyIndex = 0;
            inputHistory.Insert(0, text);
        }

        public override void OnGUI()
        {
            var pos = Event.current.mousePosition;
            var cpos = console.Window;
            GUI.Label(new Rect(10, 10, 500, 200), $"x: {pos.x - cpos.x:.###} y: {pos.y - cpos.y:.###}");
        }

        public override void Update()
        {
            if (Input.GetKeyDown(KeyCode.BackQuote) && this.console != null)
            {
                keysDown.Add(KeyCode.BackQuote);
                this.console.Visible = !this.console.Visible;
                this.consoleAutocomplete.Visible = this.console.Visible;
                if (this.console.Visible)
                {
                    takeFocus = true;
                    this.gotoEnd = true;
                }
            }

            if (this.consoleAutocomplete != null)
            {
                this.consoleAutocomplete.Window.x = this.console.Window.x;
                this.consoleAutocomplete.Window.y = this.console.Window.y + this.console.Window.height;
                this.consoleAutocomplete.Window.height = 10;
            }
        }

        public class Log
        {
            public Log(string txt)
            {
                this.txt = txt;
                this.prefix = prefix;
            }

            public Log(string txt, Color color)
            {
                this.txt = txt;
                this.prefix = prefix;
                this.color = color;
            }

            public string txt;
            public string prefix;
            public Color color = Color.white;
        }
    }
}
