﻿using System.Linq;
using UnityEngine;

namespace TeaTop.PluginSystem
{
    public class PluginWindowBehaviour : MonoBehaviour
    {
        internal Rect _window = new Rect(10, 10, 200, 200);

        internal static bool _Visible = false;

        public static float nextSave = 1f;
        public static bool Visible
        {
            get => _Visible;
            set
            {
                foreach (PluginWindow menu in PluginWindowManager.Windows.Values)
                {
                    if (!menu.Visible || menu.Window == null)
                        continue;

                    menu.OnToggleMenu?.Invoke(menu.Visible);
                }

                _Visible = value;
            }
        }

        private void Start()
        {
            PluginWindowManager.LoadRectSettings("_PluginWindowManager_MainWindow", ref this._window, ref _Visible);
            Logging.LogInfo("PluginWindowBehaviour Start");
        }

        public void Update()
        {
            nextSave -= Time.deltaTime;
            if (nextSave <= 0f)
            {
                PluginWindowManager.SaveWindowSettingsToFile();
                nextSave = 5f;
            }

            if (Input.GetKeyDown(KeyCode.Home))
            {
                Visible = !Visible;
            }
        }

        private static GUIStyle MainWindowStyle => new GUIStyle(GUI.skin.window)
        {
            normal =
            {
                background = TeaUtils.MakeTex(new Color(0.34f, 0.33f, 0.33f, 0.25f)),
            },
            onHover =
            {
                background = TeaUtils.MakeTex(new Color(0.34f, 0.33f, 0.33f, 0.65f)),
            },
            onFocused =
            {
                background = TeaUtils.MakeTex(new Color(0.34f, 0.33f, 0.33f, 0.85f))
            },
            richText = true,
        };

        public static bool SettingsLoaded = false;

        public static Rect FixRect(Rect r)
        {
            if (r.x > Screen.width - 25)
            {
                r.x = Screen.width - 25;
            }
            if (r.x < -r.width + 25)
            {
                r.x = -r.width + 25;
            }
            if (r.y > Screen.height - 25)
            {
                r.y = Screen.height - 25;
            }
            if (r.y < -r.height + 25)
            {
                r.y = -r.height + 25;
            }

            return r;
        }

        public void OnGUI()
        {
            if (Visible)
                _window = GUILayout.Window(0, _window, Main_Window, "Plugin Windows", MainWindowStyle);

            GUISkin originalSkin = GUI.skin;

            foreach (PluginWindow menu in PluginWindowManager.Windows.Values)
            {
                menu.Window = FixRect(menu.Window);

                if (!menu.Visible || (!Visible && !menu.NoButton))
                    continue;

                if (menu.GUISkin != null)
                    GUI.skin = menu.GUISkin;

                GUI.SetNextControlName(menu.Identifier);
                if (menu.GUILayout)
                    menu.Window = GUILayout.Window(menu.windowID, (Rect) menu.Window, Side_Window, menu.Title, menu.WindowStyle ?? GUI.skin.window, GUILayout.MinWidth(menu.DefaultWindow.width), GUILayout.MinHeight(menu.DefaultWindow.height));
                else
                    menu.Window = GUI.Window(menu.windowID, menu.Window, Side_Window, menu.Title, menu.WindowStyle ?? GUI.skin.window);

                GUI.skin = originalSkin;
            }

            Event canvasEvent = Event.current;
            if (EventType.MouseDown == canvasEvent.type ||
                EventType.MouseUp == canvasEvent.type ||
                EventType.MouseDrag == canvasEvent.type ||
                EventType.MouseMove == canvasEvent.type)
            {
                PluginWindowManager.focusedWindow = null;
            }
        }

        private void Main_Window(int id)
        {
            foreach (PluginWindow menu in (from val in PluginWindowManager.Windows.Values orderby val.windowID ascending select val))
            {
                if (menu.NoButton)
                    continue;

                if (GUILayout.Button(menu.ButtonName))
                {
                    menu.Visible = !menu.Visible;
                    menu.OnToggleMenu?.Invoke(menu.Visible);
                }
            }

            //GUILayout.Space(10);
            if (GUILayout.Button("Reset Window Positions"))
            {
                foreach (PluginWindow window in PluginWindowManager.Windows.Values)
                {
                    if (window.Window != null)
                    {
                        Rect r = window.Window;
                        r.x = Screen.width / 2;
                        r.y = Screen.height / 2;
                        window.Window = r;
                    }
                }
            }

            if (Application.isFocused)
                GUI.DragWindow();
        }

        private void Side_Window(int id)
        {
            PluginWindow menu = PluginWindowManager.Windows.Values.FirstOrDefault(a => a.windowID == id);

            if (menu == null)
                return;

            Event windowEvent = Event.current;
            if (EventType.MouseDown == windowEvent.type || EventType.MouseUp == windowEvent.type || EventType.MouseDrag == windowEvent.type || EventType.MouseMove == windowEvent.type)
            {
                PluginWindowManager.lastFocusedWindow = menu;
                PluginWindowManager.focusedWindow = menu;
            }

            //var mousePos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

            menu.Hovered = menu.Window.Contains(GUIUtility.GUIToScreenPoint(Event.current.mousePosition));

            menu.OnDraw?.Invoke(menu);

            if (menu.Draggable && Application.isFocused)
                GUI.DragWindow();
        }
    }
}
