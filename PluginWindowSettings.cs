﻿namespace TeaTop.PluginSystem
{
    public class PluginWindowSettings
    {
        public float x;
        public float y;
        public float width;
        public float height;
        public bool visible;
    }
}
