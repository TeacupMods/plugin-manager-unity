﻿using System;
using System.Collections.Generic;

namespace TeaTop.PluginSystem
{
    public class PluginCommand
    {
        public string Command;
        public string HelpText;
        public string Arguments;
        public Plugin plugin;
        public CommandManager.CommandCallbackDelegate Callback;

        public PluginCommand(string cmd, string helptxt, Plugin plugin)
        {
            this.Command = cmd;
            this.HelpText = helptxt;
            this.plugin = plugin;
        }

        public PluginCommand(string cmd, string helptxt, string args, Plugin plugin)
        {
            this.Command = cmd;
            this.HelpText = helptxt;
            this.Arguments = args;
            this.plugin = plugin;
        }

        public PluginCommand(string command, string usage, Plugin plugin, CommandManager.CommandCallbackDelegate callback)
        {
            this.Command = command;
            this.HelpText = usage;
            this.plugin = plugin;
            this.Callback = callback;
        }
    }
}
