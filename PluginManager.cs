﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TeaTop.PluginSystem
{
    public static class PluginManager
    {
        public static readonly IDictionary<Type, Plugin> PluginDictionary = new Dictionary<Type, Plugin>();
        public static List<Plugin> Plugins => PluginDictionary.Values.ToList();

        public static bool IsMethodOverride(this Type plugin, string name)
        {
            var method = plugin.GetMethod(name, ClassFinder.allFlags);
            return method != null && method.GetBaseDefinition() == method;
        }

        public static bool IsPlugin(Type plugin)
        {
            return plugin.IsSubclassOf(typeof(Plugin)) && plugin != typeof(Plugin);
        }

        public static bool HasInterface<T>(this Type plugin)
        {
            return plugin.IsSubclassOf(typeof(T));
        }

        public static bool IsPlugin<T>() where T : Plugin =>
            IsPlugin(typeof(T));

        public static bool IsLoaded(Type plugin)
        {
            return PluginDictionary.ContainsKey(plugin);
        }

        public static bool IsLoaded<T>() where T : Plugin =>
            IsLoaded(typeof(T));

        public static void Initialize()
        {
            foreach (Type _class in Assembly.GetAssembly(typeof(PluginManager)).GetTypes())
            {
                if (!_class.IsClass)
                    continue;

                if (PluginManager.IsPlugin(_class))
                {
                    Plugin plugin = PluginManager.LoadPlugin(_class);
                    plugin.Updating = true;
                    //ServiceManager.Start(service);
                }
            }
            Logging.LogInfo("Finished loading plugins. Running Load overrides...");
            foreach (Plugin plugin in Plugins)
            {
                try
                {
                    plugin.Load();
                }
                catch (Exception ex)
                {
                    Logging.LogException($"Error running {plugin.Name}.Load()", ex);
                }
            }


            Loader.load_object.AddComponent<PluginBehaviour>();
            PluginWindowManager.Initialize();
        }

        public static Plugin LoadPlugin(Type plugin)
        {
            if (!IsPlugin(plugin) || IsLoaded(plugin))
                return null;

            try
            {
                Logging.LogInfo($"Loading plugin {plugin.Name}...");

                Plugin instance = (Plugin) Activator.CreateInstance(plugin);
                PluginDictionary.Add(plugin, instance);

                Logging.LogSuccess($"Loaded plugin {instance.ClassName} successfuly!");
                return instance;
            }
            catch (Exception ex)
            {
                Logging.LogException($"Exception occurred while loading plugin {plugin.Name}.", ex);
            }

            return null;
        }

        public static Plugin UnloadPlugin(Type plugin)
        {
            if (!IsPlugin(plugin) || IsLoaded(plugin))
                return null;

            try
            {
                string name = plugin.Name;
                Logging.LogInfo($"Unloading plugin {name}...");

                PluginDictionary[plugin].Unload();
                PluginDictionary.Remove(plugin);

                Logging.LogSuccess($"Unloading plugin {name} successfuly!");
                return null;
            }
            catch (Exception ex)
            {
                Logging.LogException($"Exception occurred while unloading plugin {plugin.Name}.", ex);
            }

            return null;
        }

    }
}
