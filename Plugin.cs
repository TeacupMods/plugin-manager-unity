﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Harmony;

namespace TeaTop.PluginSystem
{
    public abstract class Plugin
    {
        /// <summary>
        /// Determines whether the plugin will get monobehaviour updates(Unity). 
        /// </summary>
        internal bool Updating = true;

        /// <summary>
        /// Gets the class name of the plugin.
        /// </summary>
        internal string ClassName => this.GetType().Name;

        /// <summary>
        /// The display name of the plugin. Defaults to <see cref="ClassName"/>
        /// </summary>
        public virtual string Name => ClassName;

        /// <summary>
        /// The type of the plugin. Determines whether the plugin is run clientside or serverside in some projects.
        /// </summary>
        public PluginType PluginType { get; protected set; } = PluginType.None;


        public virtual void Load()
        {
        }

        public virtual void Unload()
        {
        }

        public MethodInfo GetPatchMethod(string name)
        {
            return AccessTools.Method(this.GetType(), name);
        }

        public virtual void ThreadedUpdate()
        {

        }

        public virtual void Update()
        {

        }

        public virtual void FixedUpdate()
        {

        }

        public virtual void LateUpdate()
        {

        }

        public virtual void OnGUI()
        {

        }

        public virtual void DoPatches()
        {

        }

        public virtual IEnumerable<PluginCommand> AddCommands()
        {
            yield return null;
        }

        public virtual void RunCommand(string cmd, List<string> args, int length, string full)
        {

        }

        /*public virtual void PlayerConnected(PlayerState playerstate)
        {

        }

        public virtual void PlayerDisconnected(PlayerState playerState)
        {

        }*/

        public void AddCommand(string command, string usage)
        {
            //GameConsole.Log("Adding command " + command);
            PluginCommand cmd = new PluginCommand(command.ToLower(), usage, this);
            CommandManager.AddCommand(cmd);
        }

        public void AddCommand(string command, string usage, CommandManager.CommandCallbackDelegate action)
        {
            PluginCommand cmd = new PluginCommand(command.ToLower(), usage, this, action);
            CommandManager.AddCommand(cmd);
        }

        public PluginWindow AddWindow(string identifier, PluginWindow window)
        {
            window.Identifier = identifier;
            PluginWindowManager.AddWindow(identifier, window);
            return window;
        }

        public void AddPatch(MethodInfo method, string prefix = null, string postfix = null)
        {
            var type = MethodBase.GetCurrentMethod().DeclaringType;
            //method.HarmonyPatch(prefix != null ? type?.GetMethod(prefix) : null, prefix != null ? type?.GetMethod(postfix) : null);
        }


        public virtual void TableLoaded()
        {
        }

        public virtual void PlayerConnected(PlayerState player)
        {
        }

        public virtual void PlayerDisconnected(PlayerState player)
        {
        }
    }
}
