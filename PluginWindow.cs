﻿using UnityEngine;

namespace TeaTop.PluginSystem
{
    public class PluginWindow
    {
        internal int windowID;
        public string Identifier;
        public string Title;
        public string ButtonName;

        public bool Visible = false;
        public Rect DefaultWindow = new Rect(Screen.width / 2, Screen.height / 2, 200, 200);
        public Rect Window = new Rect(Screen.width / 2, Screen.height / 2, 200, 200);

        public float x => this.Window.x;
        public float y => this.Window.y;
        public float width => this.Window.width;
        public float height => this.Window.height;

        public GUISkin GUISkin;
        public GUIStyle WindowStyle;
        public bool Draggable = true;
        public bool NoButton = false;
        public bool GUILayout = true;

        public bool Hovered = false;

        internal delegate void ToggleWindowFunction(bool visible);
        internal delegate void DrawFunction(PluginWindow window);

        internal DrawFunction OnDraw;
        internal ToggleWindowFunction OnToggleMenu;

        /// <inheritdoc />
        public override bool Equals(object other)
        {
            if (!(other is PluginWindow))
                return false;

            PluginWindow window = (PluginWindow) other;
            return window.windowID == this.windowID;
        }

        public bool IsFocused()
        {
            return Equals(PluginWindowManager.focusedWindow, this);
        }

        public void GrabFocus()
        {
            PluginWindowManager.lastFocusedWindow = this;
            PluginWindowManager.focusedWindow = this;
            GUI.FocusWindow(this.windowID);
        }

    }
}
