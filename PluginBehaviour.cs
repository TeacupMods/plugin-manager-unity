﻿using System;
using System.Threading;
using UnityEngine;

namespace TeaTop.PluginSystem
{
    internal class PluginBehaviour : MonoBehaviour
    {
        #region Private Variables

        private Thread _ticker;

        #endregion Private Variables

        #region Mono Functions

        private void Start()
        {
            Logging.LogInfo("PluginBehaviour Start");
            _ticker = new Thread(new ThreadStart(Tick));
            _ticker.Start();
        }

        private void Update()
        {
            lock (PluginManager.PluginDictionary)
            {
                foreach (Plugin plugin in PluginManager.PluginDictionary.Values)
                {
                    if (!plugin.Updating)
                        continue;

                    plugin.Update();
                }
            }
        }

        private void FixedUpdate()
        {
            lock (PluginManager.PluginDictionary)
            {
                foreach (Plugin plugin in PluginManager.PluginDictionary.Values)
                {
                    if (!plugin.Updating)
                        continue;

                    plugin.FixedUpdate();
                }
            }
        }

        private void LateUpdate()
        {
            lock (PluginManager.PluginDictionary)
            {
                foreach (Plugin plugin in PluginManager.PluginDictionary.Values)
                {
                    if (!plugin.Updating)
                        continue;

                    plugin.LateUpdate();
                }
            }
        }

        private void OnGUI()
        {
            lock (PluginManager.PluginDictionary)
            {
                foreach (Plugin plugin in PluginManager.PluginDictionary.Values)
                {
                    if (!plugin.Updating)
                        continue;

                    try
                    {
                        plugin.OnGUI();
                    }
                    catch (Exception ex)
                    {
                        Logging.LogException($"Error occurred in {plugin.ClassName}.OnGUI", ex);
                    }
                }
            }
        }

        #endregion Mono Functions

        #region Thread Functions

        private void Tick()
        {
            lock (PluginManager.Plugins)
            {
                foreach (Plugin plugin in PluginManager.Plugins)
                {
                    if (!plugin.Updating)
                        continue;

                    plugin.ThreadedUpdate();
                }
            }
        }

        #endregion Thread Functions
    }
}