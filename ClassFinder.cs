﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TeaTop.PluginSystem
{
    public class GameClass
    {
        private static readonly BindingFlags allFlags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.SetField |
                                              BindingFlags.GetProperty | BindingFlags.SetProperty | BindingFlags.DeclaredOnly;

        public string Name;
        public Type type;
        public object singleton;
        public Type subclassOf;
        public int checksPassed = -1;
        public bool preCacheFields = false;
        public GameClassPattern pattern;

        private static readonly Dictionary<string, string> FieldNicknames = new Dictionary<string, string>();
        private static readonly Dictionary<string, string> MethodNicknames = new Dictionary<string, string>();
        private static readonly Dictionary<string, FieldInfo> CachedFields = new Dictionary<string, FieldInfo>();
        private static readonly Dictionary<string, MethodInfo> CachedMethods = new Dictionary<string, MethodInfo>();

        public FieldInfo GetField(string name)
        {
            FieldInfo ret = null;

            // Get the real(obfuscated) name of the field.
            if (FieldNicknames.ContainsKey(name))
            {
                name = FieldNicknames[name];
            }

            if (CachedFields.TryGetValue(name, out var fieldInfo))
            {
                ret = fieldInfo;
            }
            else
            {
                ret = type.GetField(name, allFlags);
                if (ret != null)
                    CachedFields[name] = ret;
            }

            return ret;
        }

        public MethodInfo GetMethod(string name)
        {
            MethodInfo ret = null;

            // Get the real(obfuscated) name of the field.
            if (MethodNicknames.ContainsKey(name))
            {
                name = MethodNicknames[name];
            }

            if (CachedMethods.TryGetValue(name, out var methodInfo))
            {
                ret = methodInfo;
            }
            else
            {
                ret = type.GetMethod(name, allFlags);

                if (ret != null)
                    CachedMethods[name] = ret;
            }

            return ret;
        }

        public T GetValue<T>(string name, object instance = null)
        {
            T ret = default(T);

            var fieldInfo = GetField(name);
            if (fieldInfo != null)
            {
                ret = (T)fieldInfo.GetValue(instance);
            }

            return ret;
        }

        public T SetValue<T>(string name, object instance = null)
        {
            T ret = default(T);

            var fieldInfo = GetField(name);
            if (fieldInfo != null)
            {
                ret = (T) fieldInfo.GetValue(instance);
            }

            return ret;
        }

        public void Invoke(string methodName, object instance, params object[] parameters)
        {
            this.type.GetMethod(methodName).Invoke(instance, parameters);
        }

        public void InvokeStatic(string methodName, params object[] parameters)
        {
            this.type.GetMethod(methodName).Invoke(null, parameters);
        }
    }

    public class GameClassPattern
    {
        /// <summary>The real name of the obfuscated class/a nickname</summary>
        public string RealName;
        /// <summary>Class must be a subclass of this type to pass the check.</summary>
        public Type baseClass;
        /// <summary>Class must be a subclass of the specified valid GameClass to pass the check.</summary>
        public string baseClassObfuscated;
        /// <summary>Number of passed checks required to consider the class what we're looking for.</summary>
        public int passedChecksRequired = 1;
        /// <summary>Number of checks that have been passed so far.</summary>
        public int passedChecks = 0;

        /// <summary>The number of fields in a class required to pass the check.</summary>
        public int numFields = -1;

        /// <summary>The number of properties in a class required to pass the check.</summary>
        public int numProperties = -1;

        /// <summary>Whether the required number of fields in the pattern can be above the specified number and still pass</summary>
        public bool numFieldsOverflow = false;

        /// <summary>Whether the required number of properties in the pattern can be above the specified number and still pass</summary>
        public bool numPropertiesOverflow = false;

        public Func<Type, bool> classCondition;
        public Func<List<FieldInfo>, bool> fieldsCondition;
        public Func<ConstructorInfo, bool> constructorCondition;

        /// <summary>A constructor's parameters in the class must be the same as the types defined in this list to pass the check.</summary>
        public List<Type> constructorParameters;

        /// <summary>A constructor's parameters in the class must contain the types defined in this list to pass the check.</summary>
        public List<Type> constructorParametersLoose;
        /// <summary>The number of fields of the specified types in this list must be met to pass the check.</summary>
        public List<Tuple<Type, int>> fieldTypesCountRequired;

        /// <summary>The types in this list must be met to pass the check.</summary>
        public List<Type> fieldTypesRequired;

        public bool Pass(bool pass)
        {
            if (pass) this.passedChecks++;
            return pass;
        }

        public void DoFieldChecks(List<FieldInfo> fields, List<Type> fieldTypes)
        {
            if (this.numFields != -1)
            {
                int fieldCount = fields.Count;
                Pass(this.numFieldsOverflow ? this.numFields >= fieldCount : this.numFields == fieldCount);
            }

            if (this.fieldTypesCountRequired != null)
            {
                bool pass = true;
                var types = this.fieldTypesCountRequired;

                foreach (Tuple<Type, int> type in types)
                {
                    int count = fields.Count(field => field.FieldType == type.Item1);
                    if (count != type.Item2)
                    {
                        pass = false;
                        break;
                    }
                }
            }
            else if (this.fieldTypesRequired != null)
            {
                bool pass = true;
                var types = this.fieldTypesRequired;

                foreach (Type type in types)
                {
                    bool ok = fieldTypes.Contains(type);
                    if (!ok)
                    {
                        pass = false;
                        break;
                    }
                }

                Pass(pass);
            }

            Pass(this.fieldsCondition.Invoke(fields));
        }

        public void DoBaseTypeCheck(Type type)
        {
            if (this.baseClass != null)
            {
                Pass(type.BaseType == this.baseClass);
            }
        }

        public void DoConstructorParameterChecks(List<ConstructorInfo> constructors)
        {
            foreach (ConstructorInfo constructor in constructors)
            {
                if (this.constructorParameters != null)
                {
                    var requiredParams = this.constructorParameters;
                    var parameters = constructor.GetParameters().ToList();
                    if (parameters.Count > 0 && requiredParams.Count == parameters.Count)
                    {
                        bool pass = true;

                        for (int i = 0; i < parameters.Count; i++)
                        {
                            if (parameters[i].ParameterType != requiredParams[i])
                            {
                                pass = false;
                                break;
                            }
                        }

                        Pass(pass);
                    }

                }
                else if (this.constructorParametersLoose != null)
                {
                    var requiredParams = this.constructorParametersLoose;
                    var parameters = constructor.GetParameters().ToList();

                    if (parameters.Count > 0 && requiredParams.Count <= parameters.Count)
                    {
                        bool pass = true;
                        var parameterTypes = (from T in parameters select T.ParameterType).ToList();

                        foreach (Type type in requiredParams)
                        {
                            bool ok = parameterTypes.Contains(type);
                            if (!ok)
                            {
                                pass = false;
                                break;
                            }
                        }

                        Pass(pass);
                    }

                }
            }
        }

        public bool ClassCondition(Type type)
        {
            return Pass(this.classCondition.Invoke(type));
        }

        public bool ConstructorCondition(ConstructorInfo constructor)
        {
            return Pass(this.constructorCondition.Invoke(constructor));
        }

        public static List<Type> TypeList<T1>()
        {
            return new List<Type>{typeof(T1)};
        }

        public static List<Type> TypeList<T1, T2>()
        {
            return new List<Type> {typeof(T1), typeof(T2)};
        }

        public static List<Type> TypeList<T1, T2, T3>()
        {
            return new List<Type> {typeof(T1), typeof(T2), typeof(T3)};
        }

        public static List<Type> TypeList<T1, T2, T3, T4>()
        {
            return new List<Type> {typeof(T1), typeof(T2), typeof(T3), typeof(T4)};
        }

        public static List<Type> TypeList<T1, T2, T3, T4, T5>()
        {
            return new List<Type> {typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5)};
        }

        public static List<Type> TypeList<T1, T2, T3, T4, T5, T6>()
        {
            return new List<Type> {typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6)};
        }
    }
    public static class ClassFinder
    {
        public static readonly Dictionary<string, GameClass> Classes = new Dictionary<string, GameClass>();
        public static readonly List<GameClassPattern> Patterns = new List<GameClassPattern>();
        public static BindingFlags allFlags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.SetField |
                                              BindingFlags.GetProperty | BindingFlags.SetProperty | BindingFlags.DeclaredOnly;

        public static Tuple<Type, int> FieldTuple<T>(T type, int num = 1)
        {
            return Tuple.Create(typeof(T), num);
        }

        public static void FindClasses(Type assemblyClass)
        {
            // Old ClassPattern system creation example
            /*ClassPattern.Create("FamilyDef",
                new Type[] {typeof(int[]), typeof(Color), typeof(string)},
                new Type[] {typeof(string), typeof(Color), typeof(bool)},
                null,
                info => info.GetParameters().Count(p => !p.ParameterType.IsPrimitive) == 4);*/

            // Old GameClassPattern system creation example
            /*new GameClassPattern()
            {
                RealName = "FamilyDef",
                fieldTypesRequired = GameClassPattern.TypeList<int[], Color, string>(),
                constructorParametersLoose = GameClassPattern.TypeList<string, Color, bool>(),
                constructorCondition = info => info.GetParameters().Count(p => !p.ParameterType.IsPrimitive) == 4,
                passedChecksRequired = 3
            };*/

            var asm = Assembly.GetAssembly(assemblyClass);

            foreach (Type _class in asm.GetTypes())
            {
                if (!_class.IsClass)
                    continue;

                var constructors = _class.GetConstructors(allFlags).ToList();
                var fields = _class.GetFields(allFlags).ToList();
                var properties = _class.GetProperties(allFlags).ToList();
                var BaseType = _class.BaseType;
                var fieldTypes = _class.GetFieldTypes().ToList();

                foreach (GameClassPattern pattern in Patterns)
                {
                    pattern.DoBaseTypeCheck(_class);
                    pattern.DoFieldChecks(fields, fieldTypes);
                    pattern.DoConstructorParameterChecks(constructors);

                    if (pattern.passedChecks >= pattern.passedChecksRequired)
                    {
                        Logging.LogInfo($"Found {pattern.RealName} class: {_class.FullName}. Checks passed: {pattern.passedChecks}/{pattern.passedChecksRequired}");
                        Patterns.Remove(pattern);
                        Classes.Add(pattern.RealName, new GameClass()
                        {
                            checksPassed = pattern.passedChecks,
                            Name = pattern.RealName,
                            pattern = pattern,
                            subclassOf = pattern.baseClass,
                            type = _class
                        });
                        //Yay success found the match
                    }
                    else
                    {
                        pattern.passedChecks = 0;
                    }
                }
            }
        }
    }
}
