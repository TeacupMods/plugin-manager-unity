﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TeaTop.PluginSystem
{
    public static class PluginWindowManager
    {
        internal static readonly Dictionary<string, PluginWindow> Windows = new Dictionary<string, PluginWindow>();
        internal static Dictionary<string, PluginWindowSettings> WindowRectSettings = new Dictionary<string, PluginWindowSettings>();
        public static bool loadedSettings = false;
        private static int currentIndex = 1;
        public static PluginWindow lastFocusedWindow;
        public static PluginWindow focusedWindow;

        public static void LoadRectSettings(string identifier, PluginWindow window)
        {
            if (!loadedSettings)
                LoadWindowSettingsFromFile();

            if (WindowRectSettings.ContainsKey(identifier))
            {
                var settings = WindowRectSettings[identifier];

                window.Window = new Rect(settings.x, settings.y, settings.width, settings.height);
                window.Visible = settings.visible;
                Logging.LogInfo("Loaded rect settings for window ", identifier);
            }
        }

        public static void LoadRectSettings(string identifier, ref Rect rect, ref bool visible)
        {
            if (!loadedSettings)
                LoadWindowSettingsFromFile();

            if (WindowRectSettings.ContainsKey(identifier))
            {
                var settings = WindowRectSettings[identifier];

                rect = new Rect(settings.x, settings.y, settings.width, settings.height);
                visible = settings.visible;
                Logging.LogInfo("Loaded rect settings for window ", identifier);
            }
        }

        public static void UpdateRectSettings(string identifier, Rect rect, bool visible)
        {
            if (!loadedSettings)
                return;

            WindowRectSettings[identifier] = new PluginWindowSettings
            {
                x = rect.x,
                y = rect.y,
                height = rect.height,
                width = rect.width,
                visible = visible
            };
        }

        public static void AddWindow(string identifier, PluginWindow window)
        {

            if (Windows.ContainsKey(identifier))
                Logging.LogWarning($"PluginWindowManager.AddWindow: Window with Identifier {identifier} already exists! This window will be overwritten!");

            LoadRectSettings(window.Identifier, window);

            window.windowID = currentIndex;
            window.Window = window.DefaultWindow;
            Windows[identifier] = window;
            Logging.LogInfo("Added Window ", window.Identifier);
            currentIndex++;
        }

        public static void LoadWindowSettingsFromFile()
        {
            var loadedWindowSettings = new Dictionary<string, PluginWindowSettings>();
            try
            {
                loadedWindowSettings = ConfigHelper.ReadConfiguration<Dictionary<string, PluginWindowSettings>>("imgui.json");
            }
            catch (Exception ex)
            {
                Logging.LogException("Error encountered while reading imgui.json. Resetting file...", ex);
                ConfigHelper.RemoveFile("imgui.json");
            }
            loadedSettings = true;
            
            if (loadedWindowSettings != null && loadedWindowSettings.Count > 0)
            {
                WindowRectSettings = loadedWindowSettings;
                Logging.LogInfo("Loaded window settings from imgui.json! Windows: ", loadedWindowSettings.Count);
            }
        }

        public static void SaveWindowSettingsToFile()
        {
            foreach (PluginWindow window in Windows.Values)
            {
                if (!String.IsNullOrEmpty(window.Identifier) && window.Window != null)
                {
                    UpdateRectSettings(window.Identifier, window.Window, window.Visible);
                }
            }

            UpdateRectSettings("_PluginWindowManager_MainWindow", behaviourInstance._window, PluginWindowBehaviour._Visible);

            ConfigHelper.WriteConfiguration("imgui.json", WindowRectSettings);
            //Logging.LogInfo("Saved window settings to imgui.json! Windows: ", WindowRectSettings.Count);
        }

        public static void Initialize()
        {
            behaviourInstance = Loader.load_object.AddComponent<PluginWindowBehaviour>();
        }

        private static PluginWindowBehaviour behaviourInstance;
    }
}
