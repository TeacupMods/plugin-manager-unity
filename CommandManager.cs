﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace TeaTop.PluginSystem
{
    public static class CommandManager
    {
        internal static readonly Dictionary<string, PluginCommand> Commands = new Dictionary<string, PluginCommand>();

        public delegate void CommandCallbackDelegate(List<string> args, int length);

        public static Dictionary<string, PluginCommand> GetCommands()
        {
            return Commands;
        }

        public static PluginCommand GetCommand(string cmd)
        {
            return Commands.ContainsKey(cmd) ? Commands[cmd] : null;
        }

        public static List<PluginCommand> GetCommandsSortedByName()
        {
            return Commands.Values.ToList().OrderBy(cmd => cmd.Command).ToList();
        }

        public static List<PluginCommand> GetCommandsSortedByLength()
        {
            return Commands.Values.ToList().OrderBy(cmd => cmd.Command.Length).ToList();
        }

        public static bool CommandExists(string command)
        {
            return Commands.ContainsKey(command);
        }

        public static bool CommandExists(PluginCommand command)
        {
            return Commands.ContainsKey(command.Command);
        }

        public static void AddCommand(PluginCommand cmd)
        {
            Commands[cmd.Command.ToLower()] = cmd;
        }

        static Regex argparse = new Regex("(?<=\")[^\"]*(?=\")|[^\" ]+", RegexOptions.Compiled);
        internal static bool HandleCommand(string text, bool prefix = false, int prefixLength = 1)
        {
            if (string.IsNullOrEmpty(text))
                return false;

            string full = prefix ? text.Substring(prefixLength) : text;
            List<string> args = argparse.Matches(full).Cast<Match>().Select(m => m.Value).ToList();

            if (args.Count == 0)
                return false;

            string cmdtxt = args[0].ToLower();

            bool ranacmd = false;
            if (Commands.ContainsKey(cmdtxt))
            {
                PluginCommand cmd = Commands[cmdtxt];
                args.RemoveAt(0);
                ranacmd = true;
                try
                {
                    cmd.Callback?.Invoke(args, args.Count);
                    cmd.plugin.RunCommand(cmdtxt, args, args.Count, full);
                }
                catch (Exception ex)
                {
                    //Logging.LogHarmony("An error ocurred while running the plugin command '" + cmd + "' with arguments '" + String.Join("|", args.ToArray()) + "'" + ex);
                    Logging.LogException("An error ocurred while running the plugin command '" + cmd.Command + "' with arguments \"" + String.Join("|", args.ToArray()) + "\"", ex);
                }
            }

            if (ranacmd)
            {
                return true;
            }

            return false;
        }
    }
}
