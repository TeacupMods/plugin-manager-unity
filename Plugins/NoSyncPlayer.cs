using System.Collections.Generic;
using System.Linq;
using Harmony;
using NewNet;
using Steamworks;
using TeaTop.Harmony.Patches;
using TeaTop.PluginSystem;
using TeaTop.Wrappers;
using UnityEngine;
using BitStream = NewNet.BitStream;
using Network = NewNet.Network;
using NetworkPlayer = NewNet.NetworkPlayer;

namespace TeaTop.Commands
{
    /// <summary>
    /// Plugin containing the following commands:
    /// - nosync: Players specified with this command will no longer be able to send RPCs or sync networked variables to the host(you). This effectively stops them from doing anything other than talking/moving their pointer.
    /// Works nicely as a "soft ban" command.
    /// - nopointersync - When toggled on, your pointer will no longer be synced to the host/players. This essentially means your pointer is inactive at wherever it last was. You cannot move objects and such while doing this.
    /// </summary>
    /// <seealso cref="TeaTop.PluginSystem.Plugin" />
    public class NoSyncPlayer : Plugin
    {
        public static List<NetworkPlayer> players = new List<NetworkPlayer>();
        public static bool noPointerSync = false;
        public static Vector3 stopPosition = Vector3.zero;

        public override void Load()
        {
            AddCommand("nosync", "<player> - Stop receiving sync/RPC packets from specified player", NoSyncCMD);
            AddCommand("nopointersync", "- Toggle pointer position sync", NoPointerSyncCMD);
        }

        private void NoPointerSyncCMD(List<string> args, int length)
        {
            noPointerSync = !noPointerSync;
            if (noPointerSync) NoSyncPlayer.stopPosition = PlayerScript.Pointer.transform.position;
            Tea.ToggleText("No Pointer Sync", noPointerSync);
        }

        private void NoSyncCMD(List<string> args, int length)
        {
            if (length == 0)
            {
                Tea.PrintHelp("nosync");
                return;
            }


            PlayerState player = TeaUtils.FindPlayer(args[0]);
            //string txt = args[1];

            if (player != null)
            {
                if (players.Contains(player.networkPlayer))
                {
                    players.Remove(player.networkPlayer);
                    Tea.PrintText("Now receiving RPC/Sync packets from " + player.name + " again.");
                }
                else
                {
                    players.Add(player.networkPlayer);
                    Tea.PrintText("No longer receiving RPC/Sync packets from " + player.name + ".");
                }
            }
            else
            {
                TeaUtils.PlayerNotFound();
                Tea.PrintHelp("nosync");
            }
        }

        public override void TableLoaded()
        {
            NoRepeat.loadedThisGame = false;
            if (players.Count > 0)
            {
                players.Clear();
                Tea.PrintText("[NoSyncPlayer.cs] Clearing nosync list.", TextColor.Info);
            }
        }

        private static bool ReceivePacket(NetworkPlayer sender, byte[] data, uint size, bool reliable, NetworkManager __instance)
        {
            var player = players.FirstOrDefault(ply => ply.id == sender.id);
            if (!player.IsValid())
            {
                return true;
            }

            BitStream stream = NetworkManager.GetStream();
            stream.Rewind();
            stream.WriteBytes(data, (int) size);
            stream.Rewind();
            NetworkHeader networkHeader = (NetworkHeader) stream.ReadByte();
            if (networkHeader == NetworkHeader.RPC || networkHeader == NetworkHeader.VarSync)
            {
                GameConsole.Log("Stopped packet from " + sender.steamID);
                return false;
            }

            return true;
        }

        private static bool SendPointerPacket(NetworkManager __instance)
        {
            if (noPointerSync)
            {
                return false;
            }

            return true;
        }

        private static bool ReceiveUpdateStuff(NetworkPlayer sender)
        {
            if (players.FirstOrDefault(x => x.id == sender.id).IsValid())
            {
                return false;
            }

            return true;
        }

        private static bool IsSamePlayer(CSteamID id1, CSteamID id2)
        {
            return id1.m_SteamID == id2.m_SteamID;
        }

        private static bool ProcessPacketStuff(byte[] data, uint msgSize, CSteamID sender, SteamP2PManager.P2PChannel channel)
        {
            NetworkPlayer sender2;
            if (Network.steamPlayers.TryGet(sender, out sender2))
            {
                if (channel != SteamP2PManager.P2PChannel.VoiceClient && channel != SteamP2PManager.P2PChannel.VoiceServer && channel != SteamP2PManager.P2PChannel.UnreliablePointer)
                {
                    if (players.FirstOrDefault(x => IsSamePlayer(x.steamID, sender)).IsValid())
                    {
                        //GameConsole.Log("1Stopped packet from " + sender);
                        return false;
                    }
                }
            }

            return true;
        }

        public override void Update()
        {
            if (Input.GetKeyDown(KeyCode.Backslash))
            {
                NoPointerSyncCMD(new List<string>(), 0);
            }
        }

        public override void OnGUI()
        {
            if (noPointerSync)
            {
                Camera camera = HoverScript.MainCamera;
                Vector3 v3 = camera.WorldToScreenPoint(stopPosition);
                if (v3.z > 0f)
                {
                    Render.DrawString(new Vector2(v3.x, Screen.height - v3.y), "Last PSync",
                        Color.magenta, true, 14, true);
                }
            }
        }

        public override void DoPatches()
        {
            AccessTools.Method(typeof(NetworkManager), "SendPointerPacket").HarmonyPatch(AccessTools.Method(typeof(NoSyncPlayer), "SendPointerPacket"));
            AccessTools.Method(typeof(SteamP2PManager), "ProcessPacket").HarmonyPatch(AccessTools.Method(typeof(NoSyncPlayer), "ProcessPacketStuff"));
        }
    }
}