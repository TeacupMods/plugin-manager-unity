﻿namespace TeaTop.PluginSystem
{
    public enum PluginType
    {
        /// <summary>Default value for plugins. Plugin possibly affects and will be run on both client/server where appropriate.</summary>
        None,
        /// <summary>Plugin will only affect the client.</summary>
        Client,
        /// <summary>Plugin will only affect the server.</summary>
        Server
    }
}
